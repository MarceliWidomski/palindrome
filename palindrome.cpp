//============================================================================
// Name        : palindrome.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

bool isPalindrome(const string& sentence);

int main() {
	cout << "Program checks if entered sentence is palindrome. Please enter sentence: " << endl;
	string sentence("");
	getline(cin, sentence);

	if (isPalindrome(sentence))
		cout << "Entered sentence is palindrome." << endl;
	else
		cout << "Entered sentence isn't palindrome." << endl;
	return 0;
}

bool isPalindrome(const string& sentence) {
	for (int i = 0, j = (sentence.length() - 1); i < j; i++, j--) {
		if (sentence[i] == ' ')
			++i;
		if (sentence[j] == ' ')
			--j;
		if (sentence[i] != sentence[j])
			return 0;
	}
	return 1;
}
